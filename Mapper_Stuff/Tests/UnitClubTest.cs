﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Entities;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitClubTest
    {
        [TestMethod]
        public void ClubPropIdTest()
        {
            //Arange
            var id = 1;
            var club = new Club();
            //Act
            club.Id = id;
            //Assert
            Assert.IsTrue(club.Id == id);
        }

        [TestMethod]
        public void ClubPropNameTest()
        {
            //Arange
            var name = "Östersjö Säl-skapet";
            var club = new Club();
            //Act
            club.Name = name;
            //Assert
            Assert.IsTrue(club.Name == name);
        }

        [TestMethod]
        public void ClubPropRegistrationDateTest()
        {
            //Arange
            var registrationDate = DateTime.Now;
            var club = new Club();
            //Act
            club.RegistrationDate = registrationDate;
            //Assert
            Assert.IsTrue(club.RegistrationDate == registrationDate);
        }

        [TestMethod]
        public void ClubPropLogoTest()
        {
            //Arange
            var logo = "https://www.pictures.google.se/";
            var club = new Club();
            //Act
            club.Logo = logo;
            //Assert
            Assert.IsTrue(club.Logo == logo);
        }

        [TestMethod]
        public void ClubPropHomePageTest()
        {
            //Arange
            var homepage = "https://www.google.se/";
            var club = new Club();
            //Act
            club.HomePage = homepage;
            //Assert
            Assert.IsTrue(club.HomePage == homepage);
        }

        [TestMethod]
        public void ClubPropDescriptionTest()
        {
            //Arange
            var description = "En klubb där sälar håller ihop.";
            var club = new Club();
            //Act
            club.Description = description;
            //Assert
            Assert.IsTrue(club.Description == description);
        }

        [TestMethod]
        public void ClubPropAddressIdTest()
        {
            //Arange
            var addressId = 1;
            var club = new Club();
            //Act
            club.AddressId = addressId;
            //Assert
            Assert.IsTrue(club.AddressId == addressId);
        }

        [TestMethod]
        public void ClubPropActiveTest()
        {
            //Arange
            var active = true;
            var club = new Club();
            //Act
            club.Active = active;
            //Assert
            Assert.IsTrue(club.Active == active);
        }

        [TestMethod]
        public void ClubPropsa_InfoTest()
        {
            //Arange
            var sa_Info = "Mer info om Säliga klubben...";
            var club = new Club();
            //Act
            club.sa_Info = sa_Info;
            //Assert
            Assert.IsTrue(club.sa_Info == sa_Info);
        }

    }
}
