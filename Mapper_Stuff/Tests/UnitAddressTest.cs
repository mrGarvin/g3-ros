﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfApplication1;
using WpfApplication1.Entities;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitAddressTest
    {
        [TestMethod]
        public void AddressPropIdTest()
        {
            //Arange
            var id = 1;
            var address = new Address();
            //Act
            address.Id = id;
            //Assert
            Assert.IsTrue(address.Id == id);
        }

        [TestMethod]
        public void AddressPropCountryTest()
        {
            //Arange
            var country = "Sweden";
            var address = new Address();
            //Act
            address.Country = country;
            //Assert
            Assert.IsTrue(address.Country == country);
        }

        [TestMethod]
        public void AddressPropCityTest()
        {
            //Arange
            var city = "Göteborg";
            var address = new Address();
            //Act
            address.City = city;
            //Assert
            Assert.IsTrue(address.City == city);
        }

        [TestMethod]
        public void AddressPropStreetTest()
        {
            //Arange
            var street = "Storgatan 12";
            var address = new Address();
            //Act
            address.Street = street;
            //Assert
            Assert.IsTrue(address.Street == street);
        }

        [TestMethod]
        public void AddressPropZipCodeTest()
        {
            //Arange
            var zipCode = "42143";
            var address = new Address();
            //Act
            address.ZipCode = zipCode;
            //Assert
            Assert.IsTrue(address.ZipCode == zipCode);
        }

        [TestMethod]
        public void AddressPropBoxNoTest()
        {
            //Arange
            var boxNo = "56734";
            var address = new Address();
            //Act
            address.BoxNo = boxNo;
            //Assert
            Assert.IsTrue(address.BoxNo == boxNo);
        }

        [TestMethod]
        public void AddressPropBoxNoNullTest()
        {
            //Arange
            var boxNo = string.Empty;
            var address = new Address();
            //Act
            address.BoxNo = boxNo;
            //Assert
            Assert.IsTrue(address.BoxNo == boxNo);
        }

        [TestMethod]
        public void AddressPropTypeTest()
        {
            //Arange
            var type = "Home-Address";
            var address = new Address();
            //Act
            address.Type = type;
            //Assert
            Assert.IsTrue(address.Type == type);
        }

        [TestMethod]
        public void AddressPropActiveTest()
        {
            //Arange
            var active = true;
            var address = new Address();
            //Act
            address.Active = active;
            //Assert
            Assert.IsTrue(address.Active == active);
        }

        [TestMethod]
        public void AddressPropInfoTest()
        {
            //Arange
            var sa_info = "Mera info om Adress...";
            var address = new Address();
            //Act
            address.sa_Info = sa_info;
            //Assert
            Assert.IsTrue(address.sa_Info == sa_info);
        }

    }
}
