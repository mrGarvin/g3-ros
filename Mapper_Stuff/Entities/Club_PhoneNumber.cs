﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Club_PhoneNumber
    {
        public int Id { get; set; }
        public int ClubId { get; set; } // FK
        public int PhoneNumberId { get; set; } // FK
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Club_PhoneNumber()
        {

        }
    }
}
