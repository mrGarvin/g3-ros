﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class RegisteredUser
    {
        public int Id { get; set; }
        public int UserId { get; set; } // FK
        public int EntryId { get; set; } // FK 
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public RegisteredUser()
        {
            
        }

    }
}
