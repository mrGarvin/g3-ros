﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Club
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Logo { get; set; }
        public string HomePage { get; set; }
        public string Description { get; set; }
        public int AddressId { get; set; } // FK
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Club()
        {
            
        }

    }
}
