﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public int TeamNo { get; set; }
        public string TeamName { get; set; }
        public int EntryId { get; set; } // FK 
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Team()
        {
            
        }
    }
}
