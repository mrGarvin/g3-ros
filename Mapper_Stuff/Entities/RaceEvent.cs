﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class RaceEvent
    {
        public int Id { get; set; } //EventId
        public string Class { get; set; }
        public string Type { get; set; }
        public int EventId { get; set; } //FK 
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public RaceEvent()
        {

        }
    }
}
