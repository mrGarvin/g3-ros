﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Regatta_Entry
    {
        public int Id { get; set; }
        public int EntryId { get; set; } //FK
        public int RegattaId { get; set; } //FK
        public DateTime RegattaEntryDate { get; set; }
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Regatta_Entry()
        {

        }
    }
}
