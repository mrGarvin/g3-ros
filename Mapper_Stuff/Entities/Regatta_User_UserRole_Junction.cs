﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Regatta_User_UserRole_Junction
    {
        public int Id { get; set; }
        public int RegattaId { get; set; } // FK
        public int UserId { get; set; } // FK
        public int UserRoleId { get; set; } // FK
        public int PhoneNumberId { get; set; } // FK
        public int EmailId { get; set; } // FK
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Regatta_User_UserRole_Junction()
        {

        }
    }
}
