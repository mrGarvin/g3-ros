﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Address
    {
        public int Id { get; set; } // Ingen set?
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string BoxNo { get; set; }
        public string Type { get; set; }
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Address()
        {
            
        }

    }
}
