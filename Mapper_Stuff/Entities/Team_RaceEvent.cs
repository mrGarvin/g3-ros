﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Team_RaceEvent
    {
        public int Id { get; set; }
        public int TeamId { get; set; } // FK
        public int RaceEventId { get; set; } // FK
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Team_RaceEvent()
        {

        }
    }
}
