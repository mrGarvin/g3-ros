﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Entry
    {
        public int Id { get; set; }
        public int EntryNo { get; set; }
        public string EntryName { get; set; }
        public string RegistrationDate { get; set; }
        public int PaidAmount { get; set; }
        public int BoatId { get; set; } //FK 
        public int ResponsibleUserId { get; set; } //FK 
        public int ClubRepresentationId { get; set; } //FK
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Entry()
        {
            
        }

    }
}
