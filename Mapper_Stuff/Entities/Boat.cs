﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Boat
    {
        public int Id { get; set; }
        public int SailNo { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Handicap { get; set; }
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Boat()
        {
            
        }
        
    }
}
