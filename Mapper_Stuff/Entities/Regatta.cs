﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Regatta
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Fee { get; set; }
        public string Description { get; set; }
        public int ClubId { get; set; } //FK 
        public int AddressId { get; set; } //FK 
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Regatta()
        {

        }

    }
}
