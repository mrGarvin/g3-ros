﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class Result
    {
        public int Id { get; set; }
        public int Rank { get; set; }
        public int Points { get; set; }
        public int Time { get; set; }
        public int Distance { get; set; }
        public int CalculatedTime { get; set; }
        public int CalculatedDistance { get; set; }
        public string Remark { get; set; }
        public int EntryId { get; set; } //FK
        public int RaceEventId { get; set; } //FK 
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public Result()
        {

        }
    }
}
