﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ICE_Name { get; set; }
        public string ICE_PhoneNumber { get; set; }
        public int PhoneNumberId { get; set; } // FK 
        public int AddressId { get; set; } // FK 
        public string Active { get; set; }
        public string sa_Info { get; set; }
        public User()
        {
            
        }

    }
}
