﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Entities
{
    public class RegisteredUser_SocialEvent
    {
        public int Id { get; set; }
        public int RegisteredUserId { get; set; } // FK
        public int SocialEventId { get; set; } // FK
        public int NoOfFriends { get; set; }
        public int PaidAmount { get; set; }
        public bool Active { get; set; }
        public string sa_Info { get; set; }
        public RegisteredUser_SocialEvent()
        {

        }

    }
}
